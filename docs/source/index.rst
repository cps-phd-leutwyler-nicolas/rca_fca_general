.. fca_algorithms documentation master file, created by
   sphinx-quickstart on Fri Sep 22 15:22:50 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to fca_algorithms's documentation!
==========================================

FCA algorithms
--------------

This is a module providing a set of commonly used algorithms in FCA, RCA, and some of its variants. Its general intention is to provide an easy to use API so that it's easier to create other programs using these algorithms. Since it's built with python, the overall performance is not expected to be outstanding. Having that said, the chosen algorithms have a somewhat low algorithmic temporal complexity.


CLI
---


FCA
###


Plot a hasse diagram from a context
***********************************

.. code-block:: bash
   

   $ fca_cli -c input.csv --show_hasse


The context is expected to be a `csv` with the following format


=====  =====  ======
name   attr1  attr2
=====  =====  ======
o1       x      
o2               x
o3       x       x  
o4                  
=====  =====  ======


Output files
############

.. code-block:: bash
   

   $ fca_cli -c input.csv --show_hasse --output_dir path/to/folder/ 


Will create two files, one representing the hasse graph, the other one with a concept for each line. The line is the index in the hasse graph.

RCA
***

To plot the hasse diagrams of the contexts 1 and 2 after applying RCA with exists

.. code-block:: bash
   

   $ fca_cli -k context_1.csv context_2.csv -r relation_1_2.csv relation_2_1.csv --show_hasse


to specify operator

.. code-block:: shell
   
   $ fca_cli -k context_1.csv context_2.csv -r relation_1_2.csv relation_2_1.csv --show_hasse -o forall




FCA utils
---------
Module for FCA basics such as retrieving concepts, drawing a hasse diagram, etc

Getting formal concepts
#######################

In batch
********

.. code-block:: python
   

   from fca.api_models import Context

   c = Context(O, A, I)
   concepts = c.get_concepts(c)


Incrementally
*************
- By intent

.. code-block:: python

   from fca.api_models import IncLattice

   l = IncLattice(attributes=['a', 'b', 'c', 'd'])
   l.add_intent('o1', [0, 2])  # numbers are the indices of the attributes
   l.add_intent('o2', [1, 2])



- By pair

.. code-block:: python

   from fca.api_models import IncLattice

   l = IncLattice()
   l.add_pair('o1', 'a')
   l.add_pair('o2', 'b')
   l.add_pair('o2', 'a')




Getting association rules
#########################

.. code-block:: python
   

   from fca.api_models import Context

   c = Context(O, A, I)
   c.get_association_rules(min_support=0.4, min_confidence=1)



Drawing hasse diagram
#####################

.. code-block:: python
   

   from fca.plot.plot import plot_from_hasse
   from fca.api_models import Context


   k = Context(O, A, I)
   k.get_lattice().plot()
   # plot receives a number of kwargs such as print_latex=True|False


   l = IncLattice(attributes=['a', 'b', 'c', 'd'])
   l.add_intent('o1', [0, 2])  # numbers are the indices of the attributes
   l.add_intent('o2', [1, 2])
   .
   .
   .
   l.plot()



Contributors
############

* Ramshell (Nicolas Leutwyler)


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   docs_tmp/getting_started


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
