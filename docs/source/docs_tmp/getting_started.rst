Getting Started
===============

.. toctree::
   :maxdepth: 4


Installation
------------

To get started with fca_algorithms, you would firstly need to download the package

.. code-block:: bash

    $ pip install fca_algorithms


To test that everything's working

.. code-block:: bash

    $ fca_cli --version



Hello world
-----------

.. code-block:: bash
    
    $ echo ",a,b\n1,x,,\n2,,x" > c.csv
    $ fca_cli -c c.csv --show_hasse

