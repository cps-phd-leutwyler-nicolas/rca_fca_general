#!/bin/bash
echo "Deleting dist/ folder"
rm -rf dist

echo "Build project"
python3 -m build

echo "Upload project"
python -m twine upload --repository pypi dist/* --verbose -u $PIP_USERNAME -p $PIP_PASSWORD
